use std::io::{self, BufRead};

fn main(){
	read_one_line();
    let numbs = read_one_line();
    let mut arr:Vec<&str>= numbs.split(" ").collect();

    let (mut tem, mut i, mut j) = (0, 0, arr.len()-1);
    while i < j {
        let tem_arr_i = arr[i];
        arr[i] = arr[j];
        arr[j] = tem_arr_i;
        i = i+1;
        j = j-1;
        //I am printing first part at the same time I order
        print!("{} ", arr[tem]);
        tem +=1 ;
    }
    //I am printing SECOND part without lose the order and starting in the middle
    while tem < arr.len() - 1 {
        print!("{} ", arr[tem]);
        tem += 1;
    }
    // Print the last element at the end avoid eveluate if and if and if in the for
    // And this increment the speed :) 
    print!("{}\n", arr[tem]);

    //To print an array use {:?} <= Interesting formater ¿ no ?
    //print!("{:?}\n", arr);
}

fn read_one_line() -> String {
	let stdin = io::stdin(); // To read the line ...
	let mut buffer = String::new();
	stdin.lock().read_line(&mut buffer).unwrap();
	buffer = buffer.trim_end().to_string();
	buffer
}


package main

import (
    "bufio"
    "fmt"
    "io"
    "os"
    "strconv"
    "strings"
)

type GroupArr struct {
   Arr [] int32
}

func (thearr GroupArr) Len() int {return len(thearr.Arr)}

func (thearr GroupArr) Less(i, j int) bool {return thearr.Arr[i] < thearr.Arr[j]}

func (thearr GroupArr) Swap(i, j int) {
    fmt.Println(">>", i, j)
    thearr.Arr[i], thearr.Arr[j] = thearr.Arr[j], thearr.Arr[i]
}

func main() {
    reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

    nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
    checkError(err)
    n := int32(nTemp)

    arrTemp := strings.Split(readLine(reader), " ")

    var arr [] int32

    for i := 0; i < int(n); i++ {
        arrItemTemp, err := strconv.ParseInt(arrTemp[i], 10, 64)
        checkError(err)
        arrItem := int32(arrItemTemp)
        arr = append(arr, arrItem)
    }

    // Sort is diferent to reverse , but is a great function to remember.
    // the func is : sort.Slice(arr, func(i, j int) bool { return arr[i] > arr[j] })
    /*  
        Reversed from python has not equivalent in Golang: This not function 
        because the reverse is from a package named SORT, and sort is to sort
        not to reverse the array as happens in python:

        In golang reverse implementation for 1 1 1 4 5 3 7
        RETURNS 7 5 4 3 1 1 1 NOT 7 3 5 4 1 1 1 as you imagine

        Review the next lines to best understanding:
        fmt.Println(arr)
        sortedArr := GroupArr{Arr: arr,}
        sort.Sort(sort.Reverse(sortedArr))
        fmt.Println(sortedArr.Arr)
    */
    tem := 0
    for i, j := 0, len(arr)-1; i < j; i, j = i+1, j-1 { 
        arr[i], arr[j] = arr[j], arr[i] 
        //I am printing first part at the same time I order
        fmt.Print(arr[tem], " ")
        tem++
    }
    //I am printing SECOND part without lose the order and starting in the middle
    for tem < len(arr) - 1 {
        fmt.Print(arr[tem], " ")
        tem++
    }
    // Print the last element at the end avoid eveluate if and if and if in the for
    // And this increment the speed :) 
    fmt.Print(arr[tem], "\n")
}

func readLine(reader *bufio.Reader) string {
    str, _, err := reader.ReadLine()
    if err == io.EOF {
        return ""
    }

    return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
    if err != nil {
        panic(err)
    }
}


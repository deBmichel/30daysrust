//URL: https://www.hackerrank.com/challenges/30-binary-numbers/problem
/*
	Playing with binaries vía algorithm.
*/
use std::io::{self, BufRead};

fn main(){
	let mut n = read_one_line().parse::<i32>().unwrap();
	let (mut mayors, mut temmayor) = (0, 0);
	
    // Use a list and play to add the remainder is a beauty exercise ...
    //let mut l:Vec<i32> = Vec::new();
    while n > 0 {
    	let remainder = n % 2;
    	n = n / 2 ;
    	//l.push(remainder);

    	if remainder == 1 {
    		temmayor += 1;
    	} else {
    		temmayor = 0;
    	}

    	if temmayor >= mayors {
    		mayors = temmayor;
    	}
    }
    //println!("{:?}", l);
    println!("{}", mayors);
	
}

fn read_one_line() -> String {
	let stdin = io::stdin(); // To read the line ...
	let mut buffer = String::new();
	stdin.lock().read_line(&mut buffer).unwrap();
	buffer = buffer.trim_end().to_string();
	buffer
}

//URL: https://www.hackerrank.com/challenges/30-binary-numbers/problem
/*
	Playing with binaries vía algorithm.
*/
package main

import (
    "bufio"
    "fmt"
    "io"
    "os"
    "strconv"
    "strings"
)

func main() {
    reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

    nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
    checkError(err)
    n, mayors, temmayor, remainder := int(nTemp), 0, 0, 0

    // Use a list and play to add the remainder is a beauty exercise ...
    // l := [] int {}
    for n > 0 {
    	remainder, n = n % 2, n / 2 
    	// l = append(l, remainder)
    	if remainder == 1 {
    		temmayor += 1
    	} else {
    		temmayor = 0
    	}

    	if temmayor >= mayors {
    		mayors = temmayor
    	}
    }
    // fmt.Println(l)
    fmt.Println(mayors)
}

func readLine(reader *bufio.Reader) string {
    str, _, err := reader.ReadLine()
    if err == io.EOF {
        return ""
    }

    return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
    if err != nil {
        panic(err)
    }
}

# URL: https://www.hackerrank.com/challenges/30-binary-numbers/problem
'''
	Playing with binaries vía algorithm.
'''
#!/bin/python3

import math
import os
import random
import re
import sys



if __name__ == '__main__':
    n, mayors, temmayor, remainder = int(input()), 0, 0, 0
    # Use a list and play to add the remainder is a beauty exercise ...
    #l=list()
    while(n > 0):
    	remainder, n = n%2, n//2
    	#l.append(remainder)
    	if remainder == 1:
    		temmayor += 1
    	else:
    		temmayor = 0

    	if temmayor >= mayors:
    		mayors = temmayor
    #print("".join(list(map(str, l))))
    print(mayors)



    

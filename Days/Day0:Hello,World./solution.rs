use std::io::{self, BufRead};

fn main() {
    let mut line = String::new();
    let stdin = io::stdin();
    stdin.lock().read_line(&mut line).unwrap();
    line = line.trim_end().to_string();
    println!("Hello, World.");
    println!("{}", line)
}
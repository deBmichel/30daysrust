package main

import (
	"bufio"
	"fmt"
	"os"
)

func main(){
	var line string

	scanner := bufio.NewScanner(os.Stdin)
	if scanner.Scan() {
        line = scanner.Text()
    }

	fmt.Println("Hello, World.\n" + line)
}

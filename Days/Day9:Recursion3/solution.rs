//URL: URL: https://www.hackerrank.com/challenges/30-recursion/problem
/*
	Topics Recursion Tactic ....
*/

use std::io::{self, BufRead};

fn factorial(n:i32) -> i32 {
	if n > 1 {
		return n * factorial(n-1);
	} else {
		return n;
	}
}

fn main(){
	let n = read_one_line().parse::<i32>().unwrap();
	let result = factorial(n);
	println!("{}", result);
}

fn read_one_line() -> String {
	let stdin = io::stdin(); // To read the line ...
	let mut buffer = String::new();
	stdin.lock().read_line(&mut buffer).unwrap();
	buffer = buffer.trim_end().to_string();
	buffer
}

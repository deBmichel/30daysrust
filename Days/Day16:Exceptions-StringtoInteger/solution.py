#url: https://www.hackerrank.com/challenges/30-exceptions-string-to-integer/problem
#!/bin/python3

import sys


S = input().strip()

try:
	n = int(S)
	print(n)
except Exception as Ex:
	print("Bad String")

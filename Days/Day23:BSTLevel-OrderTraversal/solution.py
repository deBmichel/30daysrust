#url: https://www.hackerrank.com/challenges/30-binary-trees/problem
# Printing a Binary Tree in order left to right.

import sys

class Node:
    def __init__(self,data):
        self.right=self.left=None
        self.data = data
class Solution:
    def insert(self,root,data):
        if root==None:
            return Node(data)
        else:
            if data<=root.data:
                cur=self.insert(root.left,data)
                root.left=cur
            else:
                cur=self.insert(root.right,data)
                root.right=cur
        return root

    def levelOrder(self,root):
    	# My solution has the origin in my solution to the exercise https://www.hackerrank.com/challenges/30-queues-stacks/problem
    	# in the challenge Queues and Stacks I used deque as queue, I only made a translation because the 
    	# tutorial for the challenge was very clear.
    	# Solving this exercise I remember than understanding of an algorithm is what prevails, 
    	# implementation in a language is only the sample of understanding
    	from collections import deque
    	queue = deque()
    	snds = []
    	if root :
        	queue.append(root)
        	while queue:
        		tree = queue[0]
        		queue.popleft()
        		snds.append(tree.data)

        		if tree.left:
        			queue.append(tree.left)
        		if tree.right:
        			queue.append(tree.right)

        	print(*snds)

T=int(input())
myTree=Solution()
root=None
for i in range(T):
    data=int(input())
    root=myTree.insert(root,data)
myTree.levelOrder(root)


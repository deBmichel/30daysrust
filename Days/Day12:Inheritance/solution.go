//URL: https://www.hackerrank.com/challenges/30-inheritance/problem
/*
	Inheritance
*/
package main

import "fmt"

type Person struct {
	firstName string
	lastName string
	idNumber string
}

func (p Person) printPerson () {
	fmt.Println(p.firstName)
	fmt.Println(p.lastName)
	fmt.Println(p.idNumber)
}

func (p Person) NewPerson (firstName, lastName, idNumber string) Person {
	p.firstName = firstName
	p.lastName = lastName
	p.idNumber = idNumber
	return p
}

type Student struct {
	Person
	scores [] int32
}

func (s Student) NewStudent(firstName, lastName, idNumber string, scores [] int32) {
	p := Person {}
	s.Person = p.NewPerson(firstName, lastName, idNumber)
	s.scores = scores
}

func (s Student) SumScores () int32 {
	var val int32 = 0
	for _, elmnt := range s.scores {
		val += elmnt
	}
	return val / int32(len(s.scores))
}

func (s Student) calculate () string {
	val := s.SumScores()
    if val >= 90 && val <= 100 {
    	return "O"
    }
    if val >= 80 && val < 90{
    	return "E"
    }
    if val >= 70 && val < 80{
    	return "A"
    }
    if val >= 55 && val < 70{
    	return "P"
    }
    if val >= 40 && val < 55{
    	return "D"
    }
    if val < 90{
    	return "T"
    }
    return ""
}

func main () {
    s := Student{}
    s.NewStudent("Heraldo", "Memelli", "8135627", [] int32 {100, 80})
    s.printPerson()
    fmt.Println("Grade:", s.calculate())
}

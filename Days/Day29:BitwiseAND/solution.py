#url: https://www.hackerrank.com/challenges/30-bitwise-and/problem

#!/bin/python3

import math
import os
import random
import re
import sys

"""
	I had to retry 5 times because I couldn't see beyond the linear execution, 
	I had to check the maximum values ​​to change the strategy and find an if 
	that was not evaluated in most iterations. Additionally I learned that 
	while [toIndex] is faster than [for E in range] :)
"""

if __name__ == '__main__':
	t = int(input())
	while t > 0:
		n, k = list(map(int, input().split()))
		val = 0
		r = n
		while n > 0:
			i = n-1
			while i > 0:
				op = n & i
				if op > val and op < k:
					val = op
					if val == (k - 1):
						n = -1
						break
				i -= 1
			n -= 1
		print(val)
		t -= 1


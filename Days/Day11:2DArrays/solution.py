# URL: https://www.hackerrank.com/challenges/30-2d-arrays/problem
'''
	Solving problems in bidimensional arrays , patterns
'''
#!/bin/python3

import math
import os
import random
import re
import sys



if __name__ == '__main__':
	arr = []

	for _ in range(6):
		arr.append(list(map(int, input().rstrip().split())))

	flagi, totals = len(arr) - 2, []
	for i in range(flagi):
		for subi in range(flagi):
			tem = arr[i][subi] + arr[i][subi+1] + arr[i][subi+2] + arr[i+1][subi+1]
			tem += arr[i+2][subi] + arr[i+2][subi+1] + arr[i+2][subi+2]
			totals.append(tem)
	
	print(max(totals))
	


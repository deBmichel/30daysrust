//URL: https://www.hackerrank.com/challenges/30-2d-arrays/problem
/*
	Solving problems in bidimensional arrays , patterns
*/
use std::io::{self, BufRead};

fn max_arr_i32(arr:Vec<i32>) -> i32 {
    let (mut max, mut i) = (arr[0], 0);
    let flagarr = arr.len();
    while i < flagarr {
        if arr[i] > max {
            max = arr[i];
        }
        i += 1;
    }
    max
}

fn main(){
	let mut arr:Vec<Vec<i32>> = Vec::new();
	let mut n = 0;

	while n < 6 {
		let info = read_one_line();
    	let infoarr:Vec<&str> = info.split(" ").collect();
    	let mut tem:Vec<i32> = Vec::new();

		let mut i= 0;
		while i < infoarr.len() {
			tem.push(infoarr[i].parse::<i32>().unwrap());
			i += 1;	
		}

		arr.push(tem);
    	n += 1;
    }

    let flagi = arr.len() - 2;
    let mut totals:Vec<i32> = Vec::new();
    let mut i = 0;
	while i < flagi {
		let mut subi = 0;
		while subi < flagi {
			let mut tem:i32 = arr[i][subi] + arr[i][subi+1] + arr[i][subi+2] + arr[i+1][subi+1];
			tem += arr[i+2][subi] + arr[i+2][subi+1] + arr[i+2][subi+2];
			totals.push(tem);
			subi += 1;
		}
		i += 1;
	}

	println!("{}", max_arr_i32(totals));
}

fn read_one_line() -> String {
	let stdin = io::stdin(); // To read the line ...
	let mut buffer = String::new();
	stdin.lock().read_line(&mut buffer).unwrap();
	buffer = buffer.trim_end().to_string();
	buffer
}


//URL: https://www.hackerrank.com/challenges/30-2d-arrays/problem
/*
	Solving problems in bidimensional arrays , patterns
*/
package main

import (
    "bufio"
    "fmt"
    "io"
    "os"
    "strconv"
    "strings"
)


func MaxArrI32(arr [] int32) int32 {
    max := arr[0]
    for _, element := range arr {
        if element > max {
            max = element
        }
    }
    return max
}


func main() {
    reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

    var arr [][]int32
    for i := 0; i < 6; i++ {
        arrRowTemp := strings.Split(readLine(reader), " ")

        var arrRow []int32
        for _, arrRowItem := range arrRowTemp {
            arrItemTemp, err := strconv.ParseInt(arrRowItem, 10, 64)
            checkError(err)
            arrItem := int32(arrItemTemp)
            arrRow = append(arrRow, arrItem)
        }

        if len(arrRow) != int(6) {
            panic("Bad input")
        }

        arr = append(arr, arrRow)
    }

    var flagi int = len(arr) - 2
    totals := []int32 {}
	for i:= 0 ;i < flagi; i++ {
		for subi := 0; subi < flagi; subi++ {
			tem := arr[i][subi] + arr[i][subi+1] + arr[i][subi+2] + arr[i+1][subi+1]
			tem += arr[i+2][subi] + arr[i+2][subi+1] + arr[i+2][subi+2]
			totals = append(totals, tem)
		}
	}

	fmt.Println(MaxArrI32(totals))
}

func readLine(reader *bufio.Reader) string {
    str, _, err := reader.ReadLine()
    if err == io.EOF {
        return ""
    }

    return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
    if err != nil {
        panic(err)
    }
}


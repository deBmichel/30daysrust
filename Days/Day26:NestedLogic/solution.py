#url: https://www.hackerrank.com/challenges/30-nested-logic/problem
import datetime
day1,month1,year1=list(map(int, input().split()))
day2,month2,year2=list(map(int, input().split()))

dt_return = datetime.datetime(year1,month1,day1)
dt_expected = datetime.datetime(year2,month2,day2)
delta = (dt_return - dt_expected)
days = delta.days
months = (dt_return.year - dt_expected.year) * 12 + (dt_return.month - dt_expected.month)

"""
	I had to try several times, because by underestimating 
	the exercise I omitted scenarios and important points
	to review as print(0)
"""

if year1 > year2:
    print(10000)
elif year2 > year1:
    print(0)
elif month1 > month2:
    if months > 0:
        print(500 * months)
    else:
        print(15 * days)
elif month2 > month1:
    print(0)
else:
    if day1 > day2:
        print(15 * days)
    else:
        print(0)


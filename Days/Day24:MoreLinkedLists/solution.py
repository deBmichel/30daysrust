#url:https://www.hackerrank.com/challenges/30-linked-list-deletion/problems
class Node:
	def __init__(self,data):
		self.data = data
		self.next = None 
class Solution: 
	def insert(self,head,data):
			p = Node(data)		   
			if head==None:
				head=p
			elif head.next==None:
				head.next=p
			else:
				start=head
				while(start.next!=None):
					start=start.next
				start.next=p
			return head  
	def display(self,head):
		current = head
		while current:
			print(current.data,end=' ')
			current = current.next

	def removeDuplicates(self,head):
		# This trick with while True if from my solution to https://www.hackerrank.com/challenges/30-sorting/problem
		# The input was not decreasing never.
		repeated = True
		while repeated:
			repeated = False
			current = head
			while current:
				if current.next: 
					if current.next.data == current.data:
						current.next = current.next.next
						repeated = True
				current = current.next
		return head

mylist= Solution()
T=int(input())
head=None
for i in range(T):
	data=int(input())
	head=mylist.insert(head,data)	
head=mylist.removeDuplicates(head)
mylist.display(head); 
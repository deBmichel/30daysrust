use std::io::{self, BufRead};

fn main(){
	let n = read_one_line().parse::<i32>().unwrap();
	let mut i:i32 = 1;
	while i<=10 {
    	println! ("{} x {} = {}", n, i, n*i);
    	i += 1;
  	}
}

fn read_one_line() -> String {
	let stdin = io::stdin(); // To read the line ...
	let mut buffer = String::new();
	stdin.lock().read_line(&mut buffer).unwrap();
	buffer = buffer.trim_end().to_string();
	buffer
}


use std::io::{self, BufRead};

fn main(){
	let meal_cost = read_one_line().parse::<f64>().unwrap();
	let tip_percent_tem = read_one_line().parse::<i32>().unwrap();
	let tax_percent_tem = read_one_line().parse::<i32>().unwrap();
	solvewitherror(meal_cost, tip_percent_tem, tax_percent_tem);	
}

fn solvewitherror(meal_cost:f64, tip_percent:i32, tax_percent:i32){
	let pretip = (meal_cost * ((tip_percent) as f64 / (100) as f64)) as f64;
    let pretax = (meal_cost * ((tax_percent) as f64 / (100) as f64)) as f64;

	let tip = (((pretip * (100) as f64)) as i32) as f64 / (100) as f64;
	println!("{}", tip);
    let tax = (((pretax * (100) as f64)) as i32) as f64 / (100) as f64;
    println!("{}", tax);
    println!("{}", meal_cost);
    let t = meal_cost + tip + tax;

    println!("{}", t);
    let total = (meal_cost + tip + tax).round();
    println!("{}", total);
}

fn solve(meal_cost:f64, tip_percent:i32, tax_percent:i32) {
	let tip = (meal_cost * ((tip_percent) as f64 / (100) as f64)) as f64;
    let tax = (meal_cost * ((tax_percent) as f64 / (100) as f64)) as f64;
    let total = (meal_cost + tip + tax).round() as i64;
    println!("{}", total);
}

fn read_one_line() -> String {
	let stdin = io::stdin(); // To read the line ...
	let mut buffer = String::new();
	stdin.lock().read_line(&mut buffer).unwrap();
	buffer = buffer.trim_end().to_string();
	buffer
}

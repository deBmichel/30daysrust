//Problem URL = https://www.hackerrank.com/challenges/30-operators/problem
package main

import (
    "bufio"
    "fmt"
    "io"
    "os"
    "strconv"
    "strings"
    "math"
)

// Complete the solve function below.
func solvewitherrors(meal_cost float64, tip_percent int32, tax_percent int32) {
    //Interesting tips https://stackoverflow.com/questions/18390266/how-can-we-truncate-float64-type-to-a-particular-precision
    //Not use float to play with money my friend https://stackoverflow.com/questions/3730019/why-not-use-double-or-float-to-represent-currency

    //For this exercise rounding as in python ??
    pretip := float64(meal_cost * (float64(tip_percent) / float64(100)))
    tip := float64(int(pretip*100)) / 100

    pretax := float64(meal_cost * (float64(tax_percent) / float64(100)))
    tax := float64(int(pretax*100)) / 100

    total := math.Round(meal_cost + tip + tax)
    fmt.Println(total)

    //Review this line ...
    //fmt.Println(pretip, tip, pretax, tax, total)
}

func solve(meal_cost float64, tip_percent int32, tax_percent int32) {
    tip := float64(meal_cost * (float64(tip_percent) / float64(100)))
    tax := float64(meal_cost * (float64(tax_percent) / float64(100)))
    total := math.Round(meal_cost + tip + tax)
    fmt.Println(total)
}

func main() {
    reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

    meal_cost, err := strconv.ParseFloat(readLine(reader), 64)
    checkError(err)

    tip_percentTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
    checkError(err)
    tip_percent := int32(tip_percentTemp)

    tax_percentTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
    checkError(err)
    tax_percent := int32(tax_percentTemp)

    solve(meal_cost, tip_percent, tax_percent)
}

func readLine(reader *bufio.Reader) string {
    str, _, err := reader.ReadLine()
    if err == io.EOF {
        return ""
    }

    return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
    if err != nil {
        panic(err)
    }
}


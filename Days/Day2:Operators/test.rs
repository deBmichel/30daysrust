use std::any::type_name;

// Function to print the type of a variable
fn type_of<T>(_: T) -> &'static str {
    type_name::<T>()
}

fn main() {
	let (a,b,c) = (1.74f64, 0.51f64, 10.25f64);
	let d = a + b + c;
	println!("rust obtain: {}", d);
	println!("with type {}",type_of(d));
	let e = d.round();
	println!("rust d.round(): {}", e);
	println!("with type {}",type_of(e));
}


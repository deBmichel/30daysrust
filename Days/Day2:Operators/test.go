package main

import(
	"fmt"
	"math"
)

func main () {
	a,b,c := float64(1.74), float64(0.51), float64(10.25)
	d := a+b+c
	fmt.Println("golang obtain:",d)
	fmt.Printf ("with type %T \n",d)
	e := math.Round(d)
	fmt.Println("golang math.Round(d) obtain:", e)
	fmt.Printf ("with type %T \n", e)
}



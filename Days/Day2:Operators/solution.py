#Problem URL = https://www.hackerrank.com/challenges/30-operators/problem
#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the solve function below.
def solvewitherrors(meal_cost, tip_percent, tax_percent):
	"""
		for the input:
		12.00
		20
		8

		The Round idea was :
	 	2.4000000000000004 2.4 0.96 0.96 
	 	with total = 15

	 	Bad this failed for:
	 	10.25
		17
		5
		with total = 12
		because:
		The expected output was 
		13

		The problem had the line :
		Note: Be sure to use precise values for your calculations, or you may end up with an incorrectly rounded result!
 	"""
	tip = round(meal_cost * (tip_percent / 100), 2)
	tax = round(meal_cost * (tax_percent / 100), 2)
	#Final round >><near lowest int(FOR ME)><<
	total = round(meal_cost + tip + tax)
	print(total)

def solve(meal_cost, tip_percent, tax_percent):
	tip = meal_cost * (tip_percent / 100)
	tax = meal_cost * (tax_percent / 100)
	total = round(meal_cost + tip + tax)
	print(total)

if __name__ == '__main__':
    meal_cost = float(input())

    tip_percent = int(input())

    tax_percent = int(input())

    solve(meal_cost, tip_percent, tax_percent)


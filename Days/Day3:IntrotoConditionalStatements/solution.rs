use std::io::{self, BufRead};

fn main(){
	let n = read_one_line().parse::<i32>().unwrap();
	if n % 2 == 1 {
    	println!("Weird");
  } else {
    	if n >= 2 && n <= 5 {
    		println!("Not Weird");
    	}
    	if n >= 6 && n <= 20 {
    		println!("Weird");
    	} 
   		else {
   			if n > 20 {
   				println!("Not Weird");
   		  }
      }
    }
}

fn read_one_line() -> String {
	let stdin = io::stdin(); // To read the line ...
	let mut buffer = String::new();
	stdin.lock().read_line(&mut buffer).unwrap();
	buffer = buffer.trim_end().to_string();
	buffer
}


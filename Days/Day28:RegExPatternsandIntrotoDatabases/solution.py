#url: https://www.hackerrank.com/challenges/30-regex-patterns/problem

#!/bin/python3

import math
import os
import random
import re
import sys



if __name__ == '__main__':
    N, l = int(input()), list()

    while N > 0:
        firstName, emailID = input().split()
        x = re.search('@gmail.com$', emailID)
        if(x!=None):
            l.append(firstName)
        N-=1

    l.sort()

    for e in l:
    	print(e)

cases = int(input())
for i in range(cases):
	line = input()
	evenstr = ""
	oddsstr = ""
	i = 0
	while i < len(line):
		if i%2 == 0:
			evenstr += line[i]
		else:
			oddsstr += line[i]
		i+=1
	print(evenstr, oddsstr)
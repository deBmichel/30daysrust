package main

import (
    "bufio"
    "fmt"
    "io"
    "os"
    "strconv"
    "strings"
)


func main() {
    reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

    cases, err := strconv.ParseInt(readLine(reader), 10, 64)
    checkError(err)
    var i int64 = 0
    
    for ;i < cases; i++ {
    	line := readLine(reader)
    	var (
    		evenstr string = ""
			oddsstr string = ""	
    	)
    	for i, char := range (line) {
    		if i % 2 == 0 {
    			evenstr += string(char)
    		} else {
    			oddsstr += string(char)
    		}
    	}
    	fmt.Println(evenstr, oddsstr)
    }
}

func readLine(reader *bufio.Reader) string {
    str, _, err := reader.ReadLine()
    if err == io.EOF {
        return ""
    }

    return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
    if err != nil {
        panic(err)
    }
}

use std::io::{self, BufRead};

fn main(){
	let cases = read_one_line().parse::<i32>().unwrap();
	let mut i:i32 = 0;
	while i < cases {
		let the_str:Vec<char> = read_one_line().chars().collect();
    	let mut evenstr = String::new();
		let mut oddsstr = String::new();
		let mut idx_the_str:usize = 0;
    	while idx_the_str < the_str.len() {
    		if idx_the_str % 2 == 0 {
    			evenstr += &the_str[idx_the_str].to_string();
    		} else {
    			oddsstr += &the_str[idx_the_str].to_string();
    		}
    		idx_the_str += 1;
    	}
    	println!("{} {}",evenstr, oddsstr);
    	i +=1 ;
  	}

}

fn read_one_line() -> String {
	let stdin = io::stdin(); // To read the line ...
	let mut buffer = String::new();
	stdin.lock().read_line(&mut buffer).unwrap();
	buffer = buffer.trim_end().to_string();
	buffer
}


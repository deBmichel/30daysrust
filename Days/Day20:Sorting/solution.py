#url: https://www.hackerrank.com/challenges/30-sorting/problem

#!/bin/python3

import sys

n = int(input().strip())
a = list(map(int, input().strip().split(' ')))

swaps = 0
while True:
	sort = True
	for i in range (len(a)-1):
		if (a[i] > a[i + 1]):
			sort, swaps = False, swaps +1
			a[i + 1], a[i] = a[i], a[i + 1]
	if sort:
		break

print(f'Array is sorted in {swaps} swaps.')
print(f'First Element: {a[0]}')
print(f'Last Element: {a[-1]}')


# My brain made something in this exercise, I can not understand
# Yo nunca había sentido lo que sentí en el desarrollo de este ejercicio
# Indirectamente ví moverse los números de otra manera 
# Los ví moviéndose de un lugar a otro, los número saltaban
# de izquierda a derecha en mi imaginación, fué extraño. Sublime y 
# los números en la terminal nada más, debo recordar esto, los números
# saltaron a mis ojos, se movían de un lado a otro como en la simulación
# del juego de la vida. Y luego solo conectar la idea, instintivamente
# mi cerebro retiro el for externo, quiero saber cómo hize eso, o bueno
# cómo mi cerebro hizo eso. 
# Luego recorté dos líneas y quedó más corto, FUÉ genial me divertí un 
# montón.
#url:https://www.hackerrank.com/challenges/30-running-time-and-complexity/problem
#Calculating time for algorithms ....

# mY SOlution
"""cases = int(input())
while cases > 0 :
	num = int(input())
	if num != 1:
		i, prime = 2, True
		square = num ** 0.5
		while i <= square:
			if num % i == 0:
				prime = False
				break
			i += 1
		if not prime:
			print("Not prime")
		else:
			print("Prime")
	else:
		print("Not prime")
	cases -= 1"""

#Best Solution
cases = int(input())
while cases > 0 :
	num, i, flag = int(input()), 2, True
	if num != 1:
		while i*i <= num:
			if num % i == 0:
				print("Not prime")
				flag = False
				break
			i += 1
		if flag:
			print("Prime")
	else:
		print("Not prime")
	cases -= 1

# Learning my idea was correct sqrt(num) but implementation can be optimized.
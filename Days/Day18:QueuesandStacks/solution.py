#url: https://www.hackerrank.com/challenges/30-queues-stacks/problem

import sys

class Solution:
	# Write your code here
	def __init__(self):
		from collections import deque
		self.stack = deque()
		self.queue = deque()

	def pushCharacter(self, c):
		self.stack.appendleft(c)

	def popCharacter(self):
		head = self.stack[0]
		self.stack.popleft()
		return head

	def enqueueCharacter(self, c):
		self.queue.append(c)

	def dequeueCharacter(self):
		head = self.queue[0]
		self.queue.popleft()
		return head

	def display(self):
		print("stack:", *self.stack)
		print("queue:", *self.queue)


# read the string s
s=input()
#Create the Solution class object
obj=Solution()   

l=len(s)
# push/enqueue all the characters of string s to stack
for i in range(l):
	obj.pushCharacter(s[i])
	obj.enqueueCharacter(s[i])
#obj.display()
isPalindrome=True
'''
pop the top character from stack
dequeue the first character from queue
compare both the characters
''' 
for i in range(l // 2):
	if obj.popCharacter()!=obj.dequeueCharacter():
		isPalindrome=False
		break
#finally print whether string s is palindrome or not.
if isPalindrome:
	print("The word, "+s+", is a palindrome.")
else:
	print("The word, "+s+", is not a palindrome.")	

//URL: https://www.hackerrank.com/challenges/30-dictionaries-and-maps/problem

/*
	Playing with dictionaries and maps ?
*/
package main

import (
    "bufio"
    "fmt"
    "io"
    "os"
    "strconv"
    "strings"
)

func main () {
	reader, phone_book := bufio.NewReaderSize(os.Stdin, 1024 * 1024), make(map[string]string)
	cases, err := strconv.ParseInt(readLine(reader), 10, 64)
    checkError(err)

    var i int64 = 0
    for ;i < cases; i++ {
    	info := strings.Split(readLine(reader), (" "))
    	contact, number := info[0], info[1]
    	phone_book[contact] = number
    }

    query := readLine(reader)
    for query != "" && query != "\n"{
    	if phone_book[query] != "" {
			fmt.Printf("%s=%s\n", query, phone_book[query])
    	} else {
    		fmt.Println("Not found")
    	}
    	query = readLine(reader)
    }
}

func readLine(reader *bufio.Reader) string {
    str, _, err := reader.ReadLine()
    if err == io.EOF {
        return ""
    }

    return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
    if err != nil {
        panic(err)
    }
}

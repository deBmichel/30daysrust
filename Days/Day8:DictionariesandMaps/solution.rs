//URL: https://www.hackerrank.com/challenges/30-dictionaries-and-maps/problem

/*
	Playing with dictionaries and maps ?
*/
use std::io::{self, BufRead};
use std::collections::HashMap;

fn main(){
	let cases = read_one_line().parse::<i32>().unwrap();
	let mut phone_book = HashMap::<String, String>::new();

	let mut idx:i32 = 0;
	while idx < cases {
		let info = read_one_line();
    	let arr:Vec<&str>= info.split(" ").collect();
    	phone_book.insert( arr[0].to_string(), arr[1].to_string(),);
		idx += 1;
	}

	let mut query = read_one_line();
	while query != "" && query != "\n"{
		if phone_book.contains_key(&query) {
			println!("{}={}",query, phone_book[&query]);
		} else {
			println!("Not found");
		}
		query = read_one_line();
	}
}

fn read_one_line() -> String {
	let stdin = io::stdin(); // To read the line ...
	let mut buffer = String::new();
	stdin.lock().read_line(&mut buffer).unwrap();
	buffer = buffer.trim_end().to_string();
	buffer
}

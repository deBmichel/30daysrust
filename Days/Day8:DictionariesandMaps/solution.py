# URL: https://www.hackerrank.com/challenges/30-dictionaries-and-maps/problem
'''
	Playing with dictionaries and maps ?
'''
cases, phone_book = int(input()), dict()
for _ in range(cases):
    contact_info = input().split()
    phone_book[contact_info[0]]=contact_info[1]

query, theContacts = input(), phone_book.keys()
while query != "" and query != "\n":
    if query in theContacts:
        print (f'{query}={phone_book[query]}')
    else:
        print("Not found")
    try:
        query = input()
    except:
        break


<div align="center">
<H1>Hackerrank Challenge 30DaysOfCode : {Rust Golang Python} </H1>
![pythonlogo:](hrsrcs/pythonlog.png)&nbsp;&nbsp;![golanglogo:](hrsrcs/rustlog.png)&nbsp;&nbsp;![golanglogo:](hrsrcs/golanglog.png)
<br /><br />
This repo contains deBmichel solutions for Hackerrank's 30DaysOfCode Challenge,<br />
using the programming languajes Rust Golang Python. You can run the solutions in<br />
the folder Days using the next commands (In the linux terminal):
<br /><br /><br />
</div>

1. For **RUST** use : **rustc solution.rs && ./solution < input.txt**  //To compile the rust file and run the binary using the inputs in input.txt.

2. For **PYTHON** use: **python solution.py < input.txt**  //To run the python file using the inputs in input.txt.

3. For **GOLANG** use: **go run solution.go < input.txt**  //To run the golang file using the inputs in input.txt.

<br /><br />

In each day folder you are going to find an image for each languaje execution example and the input file **(input.txt)**.


<div align="center">
![examples:](Days/Day0:Hello,World./imagrust.png)<br />
![examples:](Days/Day1:DataTypes/imagpython.png)<br />
![examples:](Days/Day1:DataTypes/imaggo.png)<br />
</div>

<div align="center">
<br /><br />
<strong>
**** Thanks for visiting the repo, greetings and wishes for success in your code ****
</strong>
<br /><h3>Completed:</h1><br /><br />
![logo:](hrsrcs/completed.png)
<br /><br /><br />
</div>



